﻿using System;

using Xamarin.Forms;

namespace webserviceapi
{
    public partial class App : Application
    {
        public static bool UseMockDataStore = true;
        public static string BackendUrl = "https://localhost:5000";

        public App()
        {
            InitializeComponent();
            MainPage = new main();
        }
    }
}
