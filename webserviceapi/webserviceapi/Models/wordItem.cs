﻿
namespace webserviceapi.Models
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class wordItem
    {
        [JsonProperty("type")]
        public string Type { get; set; } 

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }

   public partial class wordItem
    {
        public static List<wordItem> FromJson(string json) => JsonConvert.DeserializeObject<List<wordItem>>(json);//, webserviceapi.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this wordItem self) => JsonConvert.SerializeObject(self, webserviceapi.Models.Converter.Settings);
    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
          MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
          DateParseHandling = DateParseHandling.None,
          Converters = {},
        };
    }
}
