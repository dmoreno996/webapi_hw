﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plugin.Connectivity;
using webserviceapi.Models;
using Xamarin.Forms;
using System.Net.Http;

namespace webserviceapi
{
    public partial class main : ContentPage
    {
        public main()
        {
            InitializeComponent();
        }

        private bool CheckConnection()
        {
            bool internetStatus;
            if (CrossConnectivity.Current.IsConnected == true)
                internetStatus = true;
            else
                internetStatus = false;
            return internetStatus;
        }

        private void badConnectionText()
        {
            ConnectionStatusLabel.Text = "You are not on the line";
            ConnectionStatusLabel.TextColor = Color.DarkRed;
        }

        private void goodConnectionText()
        {
            ConnectionStatusLabel.Text = "Connected to Internet via " + CrossConnectivity.Current.ConnectionTypes.First();
            ConnectionStatusLabel.TextColor = Color.DarkGreen;
        }

        void Handle_ClickedCheckConnection(object sender, System.EventArgs e)
        {
            if(CheckConnection())
            {
                goodConnectionText();
            }
            else
            {
                badConnectionText();
            }
        }

        void Handle_CompletedWordSearch(object sender, System.EventArgs e)
        {
            string word = ((Entry)sender).Text;
            if (CheckConnection())
            {
                goodConnectionText();
                wordLookUp(word);
            }
            else
            {
                DisplayAlert("WARNING", "You are not on the line!", "OK");
                badConnectionText();
            }
        }
        
        async void wordLookUp(string word)
        {
            string keyword = word.ToLower();
            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format($"https://owlbot.info/api/v2/dictionary/"+$"{keyword}"));

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("Application","application / json");
            List<wordItem> owlapiData = null;
            HttpResponseMessage response = await client.SendAsync(request);
            if(response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                if (content == "[]")
                {
                    Type.Text = "Unknown word not found.";
                    Definition.Text = "Please try another word.";
                    Example.Text = " ";
                }
                else
                {
                    owlapiData = wordItem.FromJson(content);//.Substring(1, content.Length - 2));
                    Type.Text = "Type: " + owlapiData[0].Type;
                    Definition.Text = "Definition : " + owlapiData[0].Definition;
                    Example.Text = "Example: " + owlapiData[0].Example;
                }
            }
        }
    }
}
